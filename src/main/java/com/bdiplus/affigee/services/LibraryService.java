package com.bdiplus.affigee.services;

import java.util.List;

import com.bdiplus.affigee.dto.CheckoutInputRequest;
import com.bdiplus.affigee.dto.ExtendRequest;
import com.bdiplus.affigee.dto.InputRequest;
import com.bdiplus.affigee.dto.OutputResponse;
import com.bdiplus.affigee.entity.Books;

public interface LibraryService {

	OutputResponse addBooksToLibrary(InputRequest request);

	List<Books> listAllBooks();

	OutputResponse checkoutBooks(CheckoutInputRequest request);

	OutputResponse extendDayOfReturn(ExtendRequest request);

	List<Books> searchBooks(String bookName);

}

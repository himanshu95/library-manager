package com.bdiplus.affigee.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdiplus.affigee.DateUtility;
import com.bdiplus.affigee.dto.CheckoutInputRequest;
import com.bdiplus.affigee.dto.ExtendRequest;
import com.bdiplus.affigee.dto.InputRequest;
import com.bdiplus.affigee.dto.OutputResponse;
import com.bdiplus.affigee.entity.Books;
import com.bdiplus.affigee.entity.Checkout;
import com.bdiplus.affigee.repository.CheckoutRepository;
import com.bdiplus.affigee.repository.LibraryRepository;

@Service
public class LibraryServiceImpl implements LibraryService {

	private static final Logger logger = LoggerFactory.getLogger(LibraryServiceImpl.class);
	OutputResponse msg = new OutputResponse();

	@Autowired
	private LibraryRepository libraryRepo;

	@Autowired
	private CheckoutRepository checkoutRepo;

	@Autowired
	private DateUtility dateUtility;

	Books books = new Books();
	Checkout checkout = new Checkout();

	@Override
	public OutputResponse addBooksToLibrary(InputRequest request) {
		try {

			logger.info("book Exist {}", libraryRepo.existsById(request.getIsbnNumber()));

			if (libraryRepo.existsById(request.getIsbnNumber())) {
				msg.setMessageText("Book Already Exist In the Library");
				msg.setStatusCode(201);
			} else {
				books.setIsbnNumber(request.getIsbnNumber());
				books.setAuthorName(request.getAuthorName());
				books.setBookName(request.getBookName());
				books.setActive(1);
				libraryRepo.save(books);
				msg.setMessageText("Book Added In the Library");
				msg.setStatusCode(200);
			}

		} catch (Exception e) {
			logger.info("Exception While Adding the Books to Library {}", e);
			msg.setMessageText("Could Not Store Book In Library");
			msg.setStatusCode(205);
		}
		return msg;
	}

	@Override
	public List<Books> listAllBooks() {
		try {
			return libraryRepo.findAll();
		} catch (Exception e) {
			logger.info("Exception While Fetching Books {}", e);
			return null;
		}
	}

	@Override
	public OutputResponse checkoutBooks(CheckoutInputRequest request) {
		try {

			if (checkoutRepo.existsByIsbnNumber(request.getIsbnNumber())) {
				msg.setMessageText("Already Checked Out");
				msg.setStatusCode(201);
			} else {
				checkout.setUserName(request.getUserName());
				checkout.setPhoneNumber(request.getPhoneNumber());
				checkout.setIsbnNumber(request.getIsbnNumber());
				checkout.setCheckoutDate(request.getCheckoutDate());
				checkout.setDefaultReturnDate(7);
				checkout.setExtendDay(0);

				checkout.setToBeReturnDate(dateUtility.returnDate(request.getCheckoutDate(), 7));
				checkoutRepo.save(checkout);

				msg.setMessageText("You have Checked Out Book Successfully");
				msg.setStatusCode(200);
			}

		} catch (Exception e) {
			logger.info("Exception While Checking Out {}", e);
			msg.setMessageText("Could Not Checkout Books");
			msg.setStatusCode(205);
		}

		return msg;
	}

	@Override
	public OutputResponse extendDayOfReturn(ExtendRequest request) {
		try {

			if (checkoutRepo.existsByIsbnNumber(request.getIsbnNumber())) {
				Checkout tempRecord = checkoutRepo.findByIsbnNumber(request.getIsbnNumber());

				String checkoutDay = tempRecord.getCheckoutDate();
				Integer defaultReturn = tempRecord.getDefaultReturnDate();

				String newTobeReturnDate = dateUtility.returnDate(checkoutDay,
						defaultReturn + request.getExtendedDay());

				checkoutRepo.updateRecord(newTobeReturnDate, request.getExtendedDay(), request.getIsbnNumber());

				msg.setMessageText("Record Updated");
				msg.setStatusCode(200);
			} else {
				msg.setMessageText("No Such Record Exist!!");
				msg.setStatusCode(201);
			}
		} catch (Exception e) {
			logger.info("Exception While Extending Number Of Days For Return {}", e);
			msg.setMessageText("Exception While Extending Number Of Days For Return");
			msg.setStatusCode(205);
		}

		return msg;
	}

	@Override
	public List<Books> searchBooks(String bookName) {
		try {

			if (libraryRepo.existsByBookName(bookName)) {
				return libraryRepo.findByBookName(bookName);
			} else if (libraryRepo.existsByAuthorName(bookName)) {
				return libraryRepo.findByAuthorName(bookName);
			}
		} catch (Exception e) {
			logger.info("Exception While Searching Books {}", e);

		}
		return null;

	}

}

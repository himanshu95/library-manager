package com.bdiplus.affigee.dto;

public class OutputResponse {

	private String messageText;
	private Integer statusCode;

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "OutputResponse [messageText=" + messageText + ", statusCode=" + statusCode + "]";
	}

}

package com.bdiplus.affigee.dto;

public class InputRequest {

	private String isbnNumber;
	private String bookName;
	private String authorName;

	public String getIsbnNumber() {
		return isbnNumber;
	}

	public void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	@Override
	public String toString() {
		return "InputRequest [isbnNumber=" + isbnNumber + ", bookName=" + bookName + ", authorName=" + authorName + "]";
	}

}

package com.bdiplus.affigee.dto;

public class ExtendRequest {

	private Integer extendedDay;
	private String isbnNumber;

	public Integer getExtendedDay() {
		return extendedDay;
	}

	public void setExtendedDay(Integer extendedDay) {
		this.extendedDay = extendedDay;
	}

	public String getIsbnNumber() {
		return isbnNumber;
	}

	public void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}

	@Override
	public String toString() {
		return "ExtendRequest [extendedDay=" + extendedDay + ", isbnNumber=" + isbnNumber + "]";
	}

}

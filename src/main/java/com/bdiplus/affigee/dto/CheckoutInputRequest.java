package com.bdiplus.affigee.dto;

public class CheckoutInputRequest {

	private String userName;
	private String phoneNumber;
	private String isbnNumber;
	private String checkoutDate;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getIsbnNumber() {
		return isbnNumber;
	}

	public void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}

	public String getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(String checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	@Override
	public String toString() {
		return "CheckoutInputRequest [userName=" + userName + ", phoneNumber=" + phoneNumber + ", isbnNumber="
				+ isbnNumber + ", checkoutDate=" + checkoutDate + "]";
	}

}

package com.bdiplus.affigee;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.stereotype.Component;

@Component
public class DateUtility {

	public String returnDate(String checkoutDate, Integer defaultReturn) {
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(checkoutDate));
			c.add(Calendar.DATE, defaultReturn);
			String output = sdf.format(c.getTime());
			System.out.println(output);

			return output;

		} catch (Exception e) {
			System.out.println("Exception While Conversion " + e);
			return null;
		}

	}
}

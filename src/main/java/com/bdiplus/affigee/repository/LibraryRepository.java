package com.bdiplus.affigee.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bdiplus.affigee.entity.Books;

@Repository
@Transactional
public interface LibraryRepository extends JpaRepository<Books, String> {

	boolean existsByBookName(String bookName);

	boolean existsByAuthorName(String authorName);

	List<Books> findByBookName(String bookName);

	List<Books> findByAuthorName(String authorName);

}

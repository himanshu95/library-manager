package com.bdiplus.affigee.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bdiplus.affigee.entity.Checkout;

@Repository
@Transactional
public interface CheckoutRepository extends JpaRepository<Checkout, Integer> {

	boolean existsByIsbnNumber(String isbnNumber);

	Checkout findByIsbnNumber(String isbnNumber);

	@Modifying(clearAutomatically = true)
	@Query("update Checkout set toBeReturnDate =:newTobeReturnDate, extendDay =:extendedDay where isbnNumber =:isbnNumber")
	void updateRecord(@Param("newTobeReturnDate") String newTobeReturnDate, @Param("extendedDay") Integer extendedDay,
			@Param("isbnNumber") String isbnNumber);

}

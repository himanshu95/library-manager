package com.bdiplus.affigee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bdiplus.affigee.dto.CheckoutInputRequest;
import com.bdiplus.affigee.dto.ExtendRequest;
import com.bdiplus.affigee.dto.InputRequest;
import com.bdiplus.affigee.dto.OutputResponse;
import com.bdiplus.affigee.entity.Books;
import com.bdiplus.affigee.services.LibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	private LibraryService libraryService;

	/**
	 * To Add New Book  
	 */
	@PostMapping("/addBooks")
	public OutputResponse addBooksToLibrary(@RequestBody InputRequest request) {
		return libraryService.addBooksToLibrary(request);
	}
	
	/**
	 * To List All The Books 
	 */
	@GetMapping("/listAll")
	public List<Books> listAllBooks(){
		return libraryService.listAllBooks();
	}
	
	/**
	 * To Checkout Books From Library 
	 */
	@PostMapping("/checkoutBooks")
	public OutputResponse checkoutBooks(@RequestBody CheckoutInputRequest request) {
		return libraryService.checkoutBooks(request);
	}
	
	/**
	 * Extend Day Of Return 
	 */
	@PostMapping("/extendDayOfReturn")
	public OutputResponse extendDayOfReturn(@RequestBody ExtendRequest request) {
		return libraryService.extendDayOfReturn(request);
	}
	
	/**
	 * Search Book By Name 
	 */
	@GetMapping("/searchBooks")
	public List<Books> searchBooks(@RequestParam String bookName) {
		return libraryService.searchBooks(bookName);
	}
}

package com.bdiplus.affigee.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books {

	@Id
	private String isbnNumber;
	private String bookName;
	private String authorName;
	private Integer active;

	public String getIsbnNumber() {
		return isbnNumber;
	}

	public void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Books [isbnNumber=" + isbnNumber + ", bookName=" + bookName + ", authorName=" + authorName + ", active="
				+ active + "]";
	}
	
	

}

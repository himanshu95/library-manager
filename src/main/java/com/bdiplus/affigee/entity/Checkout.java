package com.bdiplus.affigee.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Checkout {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer checkoutId;
	private String userName;
	private String phoneNumber;
	private String isbnNumber;
	private String checkoutDate;
	private Integer defaultReturnDate;
	private Integer extendDay;
	private String toBeReturnDate;
	private String returnedDate;

	public Integer getCheckoutId() {
		return checkoutId;
	}

	public void setCheckoutId(Integer checkoutId) {
		this.checkoutId = checkoutId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getIsbnNumber() {
		return isbnNumber;
	}

	public void setIsbnNumber(String isbnNumber) {
		this.isbnNumber = isbnNumber;
	}

	public String getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(String checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public Integer getDefaultReturnDate() {
		return defaultReturnDate;
	}

	public void setDefaultReturnDate(Integer defaultReturnDate) {
		this.defaultReturnDate = defaultReturnDate;
	}

	public Integer getExtendDay() {
		return extendDay;
	}

	public void setExtendDay(Integer extendDay) {
		this.extendDay = extendDay;
	}

	public String getToBeReturnDate() {
		return toBeReturnDate;
	}

	public void setToBeReturnDate(String toBeReturnDate) {
		this.toBeReturnDate = toBeReturnDate;
	}

	public String getReturnedDate() {
		return returnedDate;
	}

	public void setReturnedDate(String returnedDate) {
		this.returnedDate = returnedDate;
	}

	@Override
	public String toString() {
		return "Checkout [checkoutId=" + checkoutId + ", userName=" + userName + ", phoneNumber=" + phoneNumber
				+ ", isbnNumber=" + isbnNumber + ", checkoutDate=" + checkoutDate + ", defaultReturnDate="
				+ defaultReturnDate + ", extendDay=" + extendDay + ", toBeReturnDate=" + toBeReturnDate
				+ ", returnedDate=" + returnedDate + "]";
	}

}
